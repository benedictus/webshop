$(document).ready(function () {

    if (window.messages) {
        for(var i in window.messages) {

            $.notify({
                message: window.messages[i]
            },{
                type: 'info'
            });
        }
    }

});