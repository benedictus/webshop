package org.psypo.webshop.services.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FbPictureData {
    @JsonProperty("is_silhouette")
    private Boolean isSilhouette;

    @JsonProperty("url")
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean getIsSilhouette() {
        return isSilhouette;
    }

    public void setIsSilhouette(Boolean isSilhouette) {
        this.isSilhouette = isSilhouette;
    }

}