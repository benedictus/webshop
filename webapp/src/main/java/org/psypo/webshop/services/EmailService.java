package org.psypo.webshop.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

/**
 * @author ben
 * @version 1.0
 */
@Service
public class EmailService {


    @Autowired
    private JavaMailSender mailSender;

    @Value("spring.mail.username")
    private String sender;


    public void sendEmail(String message, String subject, String... recipients)
            throws MailException, MessagingException {
        MimeMessage email = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(email, false, "utf-8");
        email.setContent(message, "text/html");
        helper.setTo(recipients);
        helper.setSubject(subject);
        helper.setFrom(sender);
        mailSender.send(email);
    }


    public void sendEmail(SimpleMailMessage message)
            throws MailException {
        mailSender.send(message);
    }


}
