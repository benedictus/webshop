package org.psypo.webshop.services.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FbPicture {

    @JsonProperty("data")
    private FbPictureData pictureData;

    public FbPictureData getPictureData() {
        return pictureData;
    }

    public void setPictureData(FbPictureData pictureData) {
        this.pictureData = pictureData;
    }

}