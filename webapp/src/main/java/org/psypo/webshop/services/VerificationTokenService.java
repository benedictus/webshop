package org.psypo.webshop.services;

import org.psypo.webshop.repositories.VerificationTokenRepository;
import org.psypo.webshop.repositories.data.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author ben
 * @version 1.0
 */
@Service
public class VerificationTokenService {

    @Autowired
    private VerificationTokenRepository verificationTokenRepository;

    public Token findByToken(String token) {
        return verificationTokenRepository.findByToken(token);
    }

    public void remove(Token token) {
        verificationTokenRepository.delete(token);
    }

}
