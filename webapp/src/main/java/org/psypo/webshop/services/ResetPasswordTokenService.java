package org.psypo.webshop.services;

import org.psypo.webshop.repositories.ResetPasswordTokenRepository;
import org.psypo.webshop.repositories.data.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author ben
 * @version 1.0
 */
@Service
public class ResetPasswordTokenService {

    @Autowired
    private ResetPasswordTokenRepository resetPasswordTokenRepository;

    public Token findByToken(String token) {
        return resetPasswordTokenRepository.findByToken(token);
    }

    public void remove(Token token) {
        resetPasswordTokenRepository.delete(token);
    }

}
