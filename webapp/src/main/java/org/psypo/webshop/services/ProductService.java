package org.psypo.webshop.services;

import org.psypo.webshop.repositories.ProductRepository;
import org.psypo.webshop.repositories.data.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collection;

/**
 * @author ben
 * @version 1.0
 */
@Service
public class ProductService {

    private static final Integer DEFAULT_SIZE = 50;
    private final ProductRepository productRepository;

    @Autowired
    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Product findOne(String id) {
        return productRepository.findOne(id);
    }

    public Collection<Product> findProducts() {
        return findProducts(0, DEFAULT_SIZE).getContent();
    }

    public Page<Product> findProducts(Integer page, Integer size) {
        return productRepository.findProducts(
                new PageRequest(page, size,
                        new Sort(
                                Arrays.asList(
                                        new Sort.Order(Sort.Direction.DESC, "discount"),
                                        new Sort.Order(Sort.Direction.ASC, "cost"),
                                        new Sort.Order(Sort.Direction.DESC, "creationDate")
                                )
                        )
                )
        );
    }

}
