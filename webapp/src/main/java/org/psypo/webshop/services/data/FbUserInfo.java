package org.psypo.webshop.services.data;


import com.fasterxml.jackson.annotation.JsonProperty;

public class FbUserInfo implements SocialUserInfo{
    @JsonProperty("name")
    private String name;
    @JsonProperty("id")
    private String id;
    @JsonProperty("email")
    private String email;

    @JsonProperty("picture")
    private FbPicture picture;


    public FbPicture getPicture() {
        return picture;
    }

    public void setPicture(FbPicture picture) {
        this.picture = picture;
    }

    @Override
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getImageUrl() {
        if (picture != null && picture.getPictureData() != null) {
            return picture.getPictureData().getUrl();
        }
        return null;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getSocialProvider() {
        return SocialProvider.FACEBOOK.getValue();
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
