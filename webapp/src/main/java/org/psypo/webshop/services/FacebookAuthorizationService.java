package org.psypo.webshop.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.oltu.oauth2.client.OAuthClient;
import org.apache.oltu.oauth2.client.URLConnectionClient;
import org.apache.oltu.oauth2.client.request.OAuthBearerClientRequest;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.client.response.GitHubTokenResponse;
import org.apache.oltu.oauth2.client.response.OAuthResourceResponse;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.OAuthProviderType;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.types.GrantType;
import org.psypo.webshop.services.data.FbUserInfo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.IOException;

import static org.psypo.webshop.config.RequestPath.AUTHORIZE_FACEBOOK;
import static org.psypo.webshop.config.RequestPath.REDIRECT;

@Service
public class FacebookAuthorizationService {

    private final static String FACEBOOK_CURRENT_USER_ENDPOINT = "me";

    @Value("${spring.social.facebook.appId}")
    private String clientId;
    @Value("${spring.social.facebook.appSecret}")
    private String clientSecret;
    @Value("${api.facebook.scope}")
    private String fbScope;
    @Value("${api.facebook.apiUrl}")
    private String facebookApiUrl;
    @Value("${api.baseUrl}" + AUTHORIZE_FACEBOOK + REDIRECT)
    private String redirectUrl;

    private static final ObjectMapper mapper = new ObjectMapper();

    private final OAuthClient oAuthClient = new OAuthClient(new URLConnectionClient());

    public String getLocation(final String state) throws
            OAuthSystemException {
        OAuthClientRequest oAuthClientRequest = OAuthClientRequest
                .authorizationProvider(OAuthProviderType.FACEBOOK)
                .setClientId(clientId)
                .setScope(fbScope)
                .setRedirectURI(redirectUrl)
                .setParameter(OAuth.OAUTH_STATE, state)
                .buildQueryMessage();
        return oAuthClientRequest.getLocationUri();
    }

    public FbUserInfo getCurrentUser(String accessToken)
            throws OAuthSystemException, OAuthProblemException, IOException {
        OAuthClientRequest bearerClientRequest =
                new OAuthBearerClientRequest(facebookApiUrl + FACEBOOK_CURRENT_USER_ENDPOINT + "?fields=name,email,picture")
                        .setAccessToken(accessToken).buildQueryMessage();

        OAuthResourceResponse resourceResponse = oAuthClient.resource(bearerClientRequest, OAuth.HttpMethod.GET, OAuthResourceResponse.class);
        if (resourceResponse.getResponseCode() == HttpStatus.OK.value()) {
            return mapper.readValue(resourceResponse.getBody(), FbUserInfo.class);
        } else {
            throw new RuntimeException("Failed to retrieve current user");
        }
    }

    public GitHubTokenResponse authorizeCodeGrant(final String code)
            throws OAuthSystemException, OAuthProblemException {
        OAuthClientRequest.TokenRequestBuilder codeGrantBuilder = OAuthClientRequest.tokenProvider(OAuthProviderType.FACEBOOK)
                .setGrantType(GrantType.AUTHORIZATION_CODE)
                .setClientId(clientId)
                .setClientSecret(clientSecret)
                .setRedirectURI(redirectUrl)
                .setCode(code);
        OAuthClientRequest codeGrant = codeGrantBuilder.buildQueryMessage();
        return oAuthClient.accessToken(codeGrant, GitHubTokenResponse.class);
    }

    public FbUserInfo getFbUser(final String code)
            throws OAuthSystemException, OAuthProblemException, IOException {
        GitHubTokenResponse accessToken = authorizeCodeGrant(code);
        return getCurrentUser(accessToken.getAccessToken());
    }


}
