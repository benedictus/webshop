package org.psypo.webshop.services;

import org.psypo.webshop.repositories.CartRepository;
import org.psypo.webshop.repositories.ProductRepository;
import org.psypo.webshop.repositories.UserRepository;
import org.psypo.webshop.repositories.data.Cart;
import org.psypo.webshop.repositories.data.Product;
import org.psypo.webshop.repositories.data.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.Collection;

/**
 * @author ben
 * @version 1.0
 */
@Service
public class CartService {

    private static final String CART_SESSION_ATTRIBUTE = "cart";
    @Autowired
    private HttpSession session;

    private final CartRepository cartRepository;
    private final ProductRepository productRepository;
    private final UserRepository userRepository;

    @Autowired
    public CartService(CartRepository cartRepository, ProductRepository productService, UserRepository userRepository) {
        this.cartRepository = cartRepository;
        this.productRepository = productService;
        this.userRepository = userRepository;
    }

    private User getUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof User) {
            return (User) principal;
        }
        return null;
    }

    public void addToCart(Collection<String> productIds) {
        Cart currentCart = getCurrentCart();

        Iterable<Product> products = productRepository.findAllByIds(productIds);

        for (Product product : products) {
            currentCart.getProducts().add(product);
        }

        saveCurrentCart(currentCart);
    }

    private void saveCurrentCart(Cart currentCart) {
        User user = getUser();
        if (user != null) {
            Cart saved = cartRepository.save(currentCart);
            if (!saved.getId().equals(saved)) {
                currentCart = saved;
                user.setCart(currentCart);
                userRepository.save(user);
            }
        }
        session.setAttribute(CART_SESSION_ATTRIBUTE, currentCart);
    }

    public Cart getCurrentCart() {
        User user = getUser();
        if (user == null) {
            Object cart = session.getAttribute(CART_SESSION_ATTRIBUTE);
            if (cart != null && cart instanceof Cart) {
                return (Cart) cart;
            }
            return new Cart();
        } else {
            Cart cart = user.getCart();
            if (cart != null) {
                return cart;
            } else {
                cart = new Cart();
                cartRepository.save(cart);
                user.setCart(cart);
                userRepository.save(user);
                return cart;
            }
        }
    }

}
