package org.psypo.webshop.services.data;

/**
 * @author ben
 * @version 1.0
 */
public enum SocialProvider {
    FACEBOOK("facebook");

    public String getValue() {
        return value;
    }

    private final String value;

    SocialProvider(String value) {
        this.value = value;
    }
}
