package org.psypo.webshop.contollers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

/**
 * @author ben
 * @version 1.0
 */
@RestController()
public class FooBar {

    @RequestMapping("/foo")
    public String index() {
        return "foobar";
    }

    @RequestMapping("/woot")
    public String woot(HttpSession session) {
        System.out.println();
        return "xxx";
    }

}
