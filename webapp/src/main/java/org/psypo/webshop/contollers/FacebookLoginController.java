package org.psypo.webshop.contollers;

import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.psypo.webshop.repositories.data.User;
import org.psypo.webshop.services.FacebookAuthorizationService;
import org.psypo.webshop.services.UserService;
import org.psypo.webshop.services.data.FbUserInfo;
import org.psypo.webshop.services.exceptions.UserCreationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.UUID;

import static org.psypo.webshop.config.RequestPath.AUTHORIZE_FACEBOOK;
import static org.psypo.webshop.config.RequestPath.REDIRECT;
import static org.psypo.webshop.config.SessionAttributes.SOCIAL_CSRF_TOKEN;

/**
 * @author ben
 * @version 1.0
 */

@Controller
@RequestMapping(AUTHORIZE_FACEBOOK)
public class FacebookLoginController {

    private final UserService userService;
    private final FacebookAuthorizationService facebookService;


    @Autowired
    public FacebookLoginController(final UserService userService,
                                   final FacebookAuthorizationService facebookService) {
        this.userService = userService;
        this.facebookService = facebookService;
    }


    @RequestMapping
    public String redirectToFacebook(HttpSession session)
            throws IOException, OAuthSystemException {
        String state = UUID.randomUUID().toString();
        session.setAttribute(SOCIAL_CSRF_TOKEN, state);
        return "redirect:" + facebookService.getLocation(state);
    }

    @RequestMapping(REDIRECT)
    public String authorize(HttpSession session,
                            @RequestParam("code") final String code,
                            final @RequestParam("state") String state)
            throws OAuthSystemException, OAuthProblemException, IOException, UserCreationException {

        Object originalState = session.getAttribute(SOCIAL_CSRF_TOKEN);
        session.removeAttribute(SOCIAL_CSRF_TOKEN);
        if (originalState == null || !originalState.toString().equals(state)) {
            throw new IllegalStateException("CSRF validation failed");
        }

        FbUserInfo fbUser = facebookService.getFbUser(code);

        User user;
        try {
            user = userService.loadUserByUsername(fbUser.getEmail());
        } catch (UsernameNotFoundException e) {
            user = userService.createSocialUser(fbUser);
        }

        final Authentication auth = new UsernamePasswordAuthenticationToken(user, null, null);
        SecurityContextHolder.getContext().setAuthentication(auth);

        return "redirect:/";
    }

}
