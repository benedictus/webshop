package org.psypo.webshop.contollers;

import org.psypo.webshop.config.RequestPath;
import org.psypo.webshop.repositories.data.Token;
import org.psypo.webshop.repositories.data.User;
import org.psypo.webshop.services.UserService;
import org.psypo.webshop.services.VerificationTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author ben
 * @version 1.0
 */
@Controller
@RequestMapping(RequestPath.VERIFICATION)
public class UserVerificationController {

    private final UserService userService;
    private final VerificationTokenService verificationTokenService;

    @Autowired
    public UserVerificationController(UserService userService, VerificationTokenService verificationTokenService) {
        this.userService = userService;
        this.verificationTokenService = verificationTokenService;
    }

    @RequestMapping("/{token}")
    public ModelAndView verify(@PathVariable("token") String token) {
        Token verificationToken = verificationTokenService.findByToken(token);
        if (verificationToken != null) {
            User user = userService.enableUser(verificationToken.getUserId());
            if (user != null) {
                verificationTokenService.remove(verificationToken);

                final Authentication auth = new UsernamePasswordAuthenticationToken(user, null, null);
                SecurityContextHolder.getContext().setAuthentication(auth);

                return new ModelAndView("redirect:/");
            }
        }

        return new ModelAndView("redirect:/", "verificationError",token);
    }

}
