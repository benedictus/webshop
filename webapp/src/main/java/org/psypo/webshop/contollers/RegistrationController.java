package org.psypo.webshop.contollers;

import org.psypo.webshop.config.RequestPath;
import org.psypo.webshop.contollers.data.UserCredentials;
import org.psypo.webshop.services.UserService;
import org.psypo.webshop.services.exceptions.UserCreationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

/**
 * @author ben
 * @version 1.0
 */
@Controller
@RequestMapping(RequestPath.SIGNUP)
public class RegistrationController {

    private static final String REGISTRATION_VIEW = "register";

    private final UserService userService;
    private final MessageSource messageSource;

    @Autowired
    public RegistrationController(UserService userService, MessageSource messageSource) {
        this.userService = userService;
        this.messageSource = messageSource;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView signUp(
            @ModelAttribute("userCredentials") @Valid UserCredentials user,
            BindingResult result) {
        if (!result.hasErrors()) {
            if (userService.usernameExists(user.getEmail())) {
                result.addError(new FieldError("userCredentials", "email","messages.signup.error.emailExists"));
            } else {
                try {
                    userService.createUser(user.getEmail(), user.getPassword());
                } catch (UserCreationException e) {
                    result.addError(new ObjectError("userCredentials", "failed to create user"));
                }
            }
        }
        if (result.hasErrors()) {
            return new ModelAndView(REGISTRATION_VIEW); // TODO
        } else {
            return new ModelAndView("redirect:"+RequestPath.ROOT); // TODO
        }
    }

    @RequestMapping
    public ModelAndView index(UserCredentials userCredentials) {
        return new ModelAndView(REGISTRATION_VIEW, "userCredentials", userCredentials);
    }

}
