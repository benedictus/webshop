package org.psypo.webshop.contollers;

import org.psypo.webshop.config.RequestPath;
import org.psypo.webshop.repositories.data.Product;
import org.psypo.webshop.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

/**
 * @author ben
 * @version 1.0
 */
@Controller
@RequestMapping(RequestPath.PRODUCT)
public class ProductController {

    private final static String PRODUCT_VIEW = "product";
    private final static String PRODUCT_LIST_VIEW = "products";

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @RequestMapping("/{id}")
    public ModelAndView product(@PathVariable("id") String id) {
        Product product = productService.findOne(id);
        if (product == null) {
            //TODO show error message
            return new ModelAndView("redirect: "+RequestPath.ROOT);
        }
        ModelAndView view = new ModelAndView(PRODUCT_VIEW);
        view.addObject("product", product);

        return view;
    }

    @RequestMapping
    public ModelAndView index(HttpSession s) {
        ModelAndView view = new ModelAndView(PRODUCT_LIST_VIEW);
        view.addObject("products");
        view.addObject("products", productService.findProducts());
        return view;
    }


}
