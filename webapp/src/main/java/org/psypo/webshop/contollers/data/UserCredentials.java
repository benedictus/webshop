package org.psypo.webshop.contollers.data;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * DTO used for user registration
 *
 * @author ben
 * @version 1.0
 */
public class UserCredentials {

    public UserCredentials() {
    }

    public UserCredentials(String email, String password) {
        this.email = email;
        this.password = password;
    }

    @NotEmpty(message = "messages.validation.error.empty")
    @Email(message = "messages.validation.error.emailFormat")
    private String email;
    @NotEmpty
    @Size(min = 8, max = 100, message = "messages.validation.error.short")
    private String password;

    private String name;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
