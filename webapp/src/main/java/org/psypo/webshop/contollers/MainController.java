package org.psypo.webshop.contollers;

import org.psypo.webshop.config.RequestPath;
import org.psypo.webshop.repositories.data.Product;
import org.psypo.webshop.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * @author ben
 * @version 1.0
 */
@Controller
@RequestMapping(RequestPath.ROOT)
public class MainController {

    static final String MAIN_VIEW = "main";
    private final static Integer PREFERED_PRODUCT_COUNT = 5;

    public final ProductService productService;

    @Autowired
    public MainController(ProductService productService) {
        this.productService = productService;
    }

    @RequestMapping
    public ModelAndView index(HttpSession s) {
        ModelAndView view = new ModelAndView(MAIN_VIEW);
        Page<Product> response = productService.findProducts(0, PREFERED_PRODUCT_COUNT);

        Map<String,String> images = new HashMap<>();

        Random random = new Random();
        for (Product product : response.getContent()) {
            List<String> imageUrls = new ArrayList<>(product.getItem().getImageUrls());
            if (imageUrls.size() > 0) {
                int i = random.nextInt(imageUrls.size());
                images.put(product.getId(),imageUrls.get(i));
            } else if (product.getItem().getPreviewImageUrl() != null){
                images.put(product.getId(),product.getItem().getPreviewImageUrl());
            }
        }
        view.addObject("carousel", images);
        return view;
    }

}
