package org.psypo.webshop.contollers;

import org.psypo.webshop.config.RequestPath;
import org.psypo.webshop.services.ResetPasswordTokenService;
import org.psypo.webshop.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(RequestPath.RESET_PASSWORD)
public class ChangePasswordController {

    private final UserService userService;
    private final ResetPasswordTokenService resetPasswordTokenService;

    @Autowired
    public ChangePasswordController(UserService userService, ResetPasswordTokenService resetPasswordTokenService) {
        this.userService = userService;
        this.resetPasswordTokenService = resetPasswordTokenService;
    }
//
//    @RequestMapping("{email}")
//    public ModelAndView requestPasswordChange(@PathVariable("email") String email) {
//        User user = userService.loadUserByUsername(email);
//
//    }


}
