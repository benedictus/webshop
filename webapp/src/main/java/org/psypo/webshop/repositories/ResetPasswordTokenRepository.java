package org.psypo.webshop.repositories;

import org.psypo.webshop.repositories.data.Token;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author ben
 * @version 1.0
 */
public interface ResetPasswordTokenRepository extends MongoRepository<Token, String> {

    public Token findByToken(String token);

}
