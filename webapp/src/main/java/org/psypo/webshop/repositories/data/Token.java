package org.psypo.webshop.repositories.data;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.UUID;

/**
 * @author ben
 * @version 1.0
 */
@Document
public class Token {

    public Token(String userId) {
        this.token = UUID.randomUUID().toString();
        this.userId = userId;
        creationDate = new Date();
    }

    public Token() {

    }

    @Id
    private String id;
    private Date creationDate;
    @Indexed(unique = true)
    private String token;
    private String userId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
