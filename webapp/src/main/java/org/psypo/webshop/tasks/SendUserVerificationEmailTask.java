package org.psypo.webshop.tasks;

import org.apache.log4j.Logger;
import org.psypo.webshop.repositories.UserRepository;
import org.psypo.webshop.repositories.VerificationTokenRepository;
import org.psypo.webshop.repositories.data.User;
import org.psypo.webshop.repositories.data.Token;
import org.psypo.webshop.services.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.mail.MailException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.thymeleaf.context.Context;
import org.thymeleaf.context.IContext;
import org.thymeleaf.context.VariablesMap;
import org.thymeleaf.spring4.SpringTemplateEngine;

import javax.mail.MessagingException;

/**
 * @author ben
 * @version 1.0
 */
@Component
public class SendUserVerificationEmailTask {

    private static final Logger LOG = Logger.getLogger(SendUserVerificationEmailTask.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private VerificationTokenRepository verificationTokenRepository;

    @Autowired
    private EmailService service;

    @Autowired
    private SpringTemplateEngine templateEngine;

    @Autowired
    private MessageSource messageSource;

    @Value("${api.baseUrl}")
    private String baseUrl;

    @Scheduled(cron = " 0 0-59/1 * * * * ")
    public void sendVerificationEmails() {
        Page<User> users = userRepository.findUsersUnsetVerifications(new PageRequest(0, 100));
        for (User user : users.getContent()) {
            Token verificationToken = new Token(user.getId());

            IContext thc = new Context();
            VariablesMap<String, Object> variables = thc.getVariables();
            variables.put("baseUrl", baseUrl);
            variables.put("verification", verificationToken);
            String message = templateEngine.process("email/userVerification", thc);
            thc.getLocale();

            try {
                service.sendEmail(
                        message,
                        messageSource.getMessage(
                                "messages.email.verification.subject",
                                null, thc.getLocale()),
                        user.getUsername());
            } catch (MailException | MessagingException e) {
                LOG.warn(e);
                continue;
            }

            verificationTokenRepository.save(verificationToken);
            user.setVerificationSent(true);
            User save = userRepository.save(user);
        }


    }

}
