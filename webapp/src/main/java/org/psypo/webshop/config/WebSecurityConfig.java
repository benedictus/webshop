package org.psypo.webshop.config;

import org.psypo.webshop.handlers.CustomAutenticationFailureHandler;
import org.psypo.webshop.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebMvcSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserService userService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers(
                        RequestPath.ROOT,
                        RequestPath.SIGNUP,
                        RequestPath.VERIFICATION + "/**",
                        RequestPath.AUTHORIZE_FACEBOOK + "/**",
                        RequestPath.LOGIN,
                        RequestPath.PRODUCT + "/**",
                        RequestPath.RESET_PASSWORD + "/**",
                        RequestPath.PUBLIC + "/**")
                .permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin().failureHandler(new CustomAutenticationFailureHandler())
                .loginPage(RequestPath.LOGIN)
                .permitAll()
                .and()
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher(RequestPath.LOGOUT))
                        .logoutSuccessUrl(RequestPath.ROOT).deleteCookies("JSESSIONID")
                        .invalidateHttpSession(true)
                        .permitAll();

        http.sessionManagement()
                .maximumSessions(1)
                .expiredUrl("/login?error=true")
                .maxSessionsPreventsLogin(true)
                .and()
                .sessionCreationPolicy(SessionCreationPolicy.NEVER)
                .sessionFixation().none();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService).passwordEncoder(userService.getPasswordEncoder());
    }

}