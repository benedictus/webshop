package org.psypo.webshop.config;

/**
 * @author ben
 * @version 1.0
 */
public class RequestPath {
    public static final String AUTHORIZE_FACEBOOK = "/authorize/facebook";
    public static final String REDIRECT = "/redirect";
    public static final String SIGNUP = "/signup";
    public static final String VERIFICATION = "/verification";
    public static final String PRODUCT = "/product";
    public static final String PUBLIC = "/public";
    public static final String LOGIN = "/login";
    public static final String LOGOUT = "/logout";
    public static final String RESET_PASSWORD = "/resetPassword";
    public static final String ROOT = "/";
}
