package org.psypo.webshop.controllers.forms;

import org.psypo.webshop.data.ThirdPartyOrigin;
import org.psypo.webshop.repositories.data.Item;

import java.util.*;

/**
 * @author ben
 * @version 1.0
 */
public class ItemForm {

    public ItemForm() {
        creationDate = new Date();
        tags = new HashSet<>();
        imageUrls = new ArrayList<>();
    }

    public ItemForm(Item item) {
        this.id = item.getId();
        this.name = item.getName();
        this.creationDate = item.getCreationDate();
        this.tags = item.getTags();
        this.description = item.getDescription();
        this.previewImageUrl = item.getPreviewImageUrl();
        this.imageUrls = item.getImageUrls();
        if (item.getExternalLinks() != null) {
            this.facebookLink = item.getExternalLinks().get(ThirdPartyOrigin.Facebook);
            this.instagramLink = item.getExternalLinks().get(ThirdPartyOrigin.Instagram);
        }
    }

    private String id;
    private String name;
    private Set<String> tags;
    private String description;
    private String facebookLink;
    private String instagramLink;
    private String previewImageUrl;
    private Collection<String> imageUrls;
    private Date creationDate;

    public Item toItem() {
        Item item = new Item();
        if (id != null && !id.isEmpty()) {
            item.setId(id);
        }
        item.setCreationDate(creationDate);
        item.setDescription(description);
        Map<ThirdPartyOrigin, String> externalLinks = new HashMap<>();
        if (instagramLink != null && !instagramLink.isEmpty()) {
            externalLinks.put(ThirdPartyOrigin.Instagram, instagramLink);
        }
        if (facebookLink != null && !facebookLink.isEmpty()) {
            externalLinks.put(ThirdPartyOrigin.Facebook, facebookLink);
        }
        item.setExternalLinks(externalLinks);
        item.setImageUrls(imageUrls);
        item.setName(name);
        item.setTags(tags);
        item.setPreviewImageUrl(previewImageUrl);
        return item;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<String> getTags() {
        return tags;
    }

    public void setTags(Set<String> tags) {
        this.tags = tags;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFacebookLink() {
        return facebookLink;
    }

    public void setFacebookLink(String facebookLink) {
        this.facebookLink = facebookLink;
    }

    public String getInstagramLink() {
        return instagramLink;
    }

    public void setInstagramLink(String instagramLink) {
        this.instagramLink = instagramLink;
    }

    public String getPreviewImageUrl() {
        return previewImageUrl;
    }

    public void setPreviewImageUrl(String previewImageUrl) {
        this.previewImageUrl = previewImageUrl;
    }

    public Collection<String> getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(Collection<String> imageUrls) {
        this.imageUrls = imageUrls;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}
