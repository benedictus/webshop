package org.psypo.webshop.controllers.forms;

import org.psypo.webshop.repositories.data.Product;

import java.util.Date;

/**
 * @author ben
 * @version 1.0
 */
public class ProductForm {

    public ProductForm() {
        creationDate = new Date();
        active = false;
    }

    public ProductForm(Product product) {
        this.id = product.getId();
        this.cost = product.getCost();
        this.discount = product.getDiscount();
        this.stock = product.getStock();
        this.creationDate = product.getCreationDate();
        this.active = product.isActive();
        if (product.getItem() != null) {
            this.itemId = product.getItem().getId();
            this.itemName = product.getItem().getName();
        }
    }

    private String id;
    private Double cost;
    private Double discount;
    private Integer stock;
    private Date creationDate;
    private boolean active;
    private String itemId;
    private String itemName;

    public Product toProduct() {
        Product product = new Product();
        product.setId(id);
        product.setActive(active);
        product.setCost(cost);
        product.setCreationDate(creationDate);
        product.setDiscount(discount);
        product.setStock(stock);
        return product;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }
}
