package org.psypo.webshop.controllers;

import org.psypo.webshop.config.RequestPath;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author ben
 * @version 1.0
 */
@Controller
@RequestMapping(RequestPath.ROOT)
public class MainController {

    @RequestMapping
    public ModelAndView index() {
        return new ModelAndView("redirect: "+RequestPath.ITEM);
    }

}
