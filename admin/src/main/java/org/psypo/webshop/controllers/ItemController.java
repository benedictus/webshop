package org.psypo.webshop.controllers;

import org.psypo.webshop.config.RequestPath;
import org.psypo.webshop.controllers.forms.ItemForm;
import org.psypo.webshop.repositories.ItemRepository;
import org.psypo.webshop.repositories.data.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.stream.Collectors;

import static org.springframework.data.domain.Sort.Direction.DESC;

/**
 * @author ben
 * @version 1.0
 */
@Controller
@RequestMapping(RequestPath.ITEM)
public class ItemController {

    static final String LIST_VIEW = "itemList";
    static final String EDIT_VIEW = "item";

    private final ItemRepository itemRepository;

    @Autowired
    public ItemController(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView item(@ModelAttribute("item") ItemForm item) {
        Item saved = itemRepository.save(item.toItem());
        if (saved == null) {
            throw new IllegalStateException("Item was not saved");
        }
        return new ModelAndView("redirect:" + RequestPath.ITEM + "/" + saved.getId());
    }

    @RequestMapping("/create")
    public ModelAndView newItem() {
        ModelAndView view = new ModelAndView(EDIT_VIEW);
        ItemForm item = new ItemForm();
        view.addObject("item", item);
        return view;
    }

    @RequestMapping("/{id}")
    public ModelAndView item(@PathVariable("id") final String id) {
        ModelAndView view = new ModelAndView(EDIT_VIEW);
        Item item = itemRepository.findOne(id);
        if (item == null) {
            throw new IllegalStateException("No such item");
            //TODO add exception handler and
            // provide more accurate exception signature
        }
        view.addObject("item", new ItemForm(item));
        return view;
    }

    @RequestMapping("/page/{page}")
    public ModelAndView items(@PathVariable("page") final Integer page,
                              @RequestParam(value = "size", defaultValue = "10") final Integer
                                      size) {
        Page<Item> items = itemRepository.findAll(
                new PageRequest(page, size,
                        new Sort(new Order(DESC, "creationDate"))
                )
        );
        ModelAndView view = new ModelAndView(LIST_VIEW);
        view.addObject("totalPages", items.getTotalPages());
        view.addObject("page", page);
        view.addObject("items", items.getContent()
                        .stream().map(ItemForm::new).collect(Collectors.toList())
        );
        return view;
    }

    @RequestMapping
    public ModelAndView index(
            @RequestParam(value = "size", defaultValue = "10") final Integer size) {
        return items(0, size);
    }

}
