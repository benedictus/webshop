package org.psypo.webshop.controllers;

import org.psypo.webshop.config.RequestPath;
import org.psypo.webshop.controllers.forms.ProductForm;
import org.psypo.webshop.repositories.ItemRepository;
import org.psypo.webshop.repositories.ProductRepository;
import org.psypo.webshop.repositories.data.Item;
import org.psypo.webshop.repositories.data.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.stream.Collectors;

import static org.springframework.data.domain.Sort.Direction.DESC;

/**
 * @author ben
 * @version 1.0
 */
@Controller
@RequestMapping(RequestPath.PRODUCT)
public class ProductController {

    static final String LIST_VIEW = "productList";
    static final String EDIT_VIEW = "product";

    private final ItemRepository itemRepository;
    private final ProductRepository productRepository;

    @Autowired
    public ProductController(ItemRepository itemRepository, ProductRepository productRepository) {
        this.itemRepository = itemRepository;
        this.productRepository = productRepository;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView item(@ModelAttribute("item") ProductForm productForm) {
        if (productForm.getId() != null && productForm.getId().isEmpty()) {
            productForm.setId(null);
        }
        Product product = productForm.toProduct();
        if(productForm.getItemId() != null && !productForm.getItemId().isEmpty()) {
            Item item = itemRepository.findOne(productForm.getItemId());
            if (item == null) {
                throw new IllegalArgumentException("Item with id "+productForm.getItemId()+
                        " does not exist");
            }
            product.setItem(item);
        }
        Product saved = productRepository.save(product);
        if (saved == null) {
            throw new IllegalStateException("Item was not saved");
        }
        return new ModelAndView("redirect:" + RequestPath.PRODUCT + "/" + saved.getId());
    }

    @RequestMapping("/create")
    public ModelAndView newProduct() {
        ModelAndView view = new ModelAndView(EDIT_VIEW);
        ProductForm product = new ProductForm();
        view.addObject("product", product);
        view.addObject("items", itemRepository.findAll());
        return view;
    }

    @RequestMapping("/{id}")
    public ModelAndView product(@PathVariable("id") final String id) {
        ModelAndView view = new ModelAndView(EDIT_VIEW);
        Product product = productRepository.findOne(id);
        if (product == null) {
            throw new IllegalStateException("No such product");
            //TODO add exception handler and
            // provide more accurate exception signature
        }
        view.addObject("product", new ProductForm(product));
        view.addObject("items", itemRepository.findAll());
        return view;
    }

    @RequestMapping("/page/{page}")
    public ModelAndView items(@PathVariable("page") final Integer page,
                              @RequestParam(value = "size", defaultValue = "10") final Integer
                                      size) {
        Page<Product> products = productRepository.findAll(
                new PageRequest(page, size,
                        new Sort(new Order(DESC, "creationDate"))
                )
        );
        ModelAndView view = new ModelAndView(LIST_VIEW);
        view.addObject("totalPages", products.getTotalPages());
        view.addObject("page", page);
        view.addObject("products", products.getContent()
                        .stream().map(ProductForm::new).collect(Collectors.toList())
        );
        return view;
    }

    @RequestMapping
    public ModelAndView index(
            @RequestParam(value = "size", defaultValue = "10") final Integer size) {
        return items(0, size);
    }

}
