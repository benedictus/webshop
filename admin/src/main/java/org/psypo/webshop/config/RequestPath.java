package org.psypo.webshop.config;

/**
 * @author ben
 * @version 1.0
 */
public class RequestPath {
    public static final String PUBLIC = "/public";
    public static final String LOGIN = "/login";
    public static final String LOGOUT = "/logout";
    public static final String ROOT = "/";
    public static final String ITEM = "/items";
    public static final String PRODUCT = "/products";

}
