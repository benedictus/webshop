package org.psypo.webshop.repositories;

import org.psypo.webshop.repositories.data.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;


public interface ProductRepository extends MongoRepository<Product, String> {

    @Query("{'active' : true, 'stock' : {$gt : 0}}")
    public Page<Product> findProducts(Pageable pageable);

    @Query("{'active' : true, 'stock' : {$gt : 0}}")
    Iterable<Product> findAllByIds(Iterable<String> ids);

}
