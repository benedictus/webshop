package org.psypo.webshop.repositories.data;

/**
 * @author ben
 * @version 1.0
 */
public class UserPersonalDetails {
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
