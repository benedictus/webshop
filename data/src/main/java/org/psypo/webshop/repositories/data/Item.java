package org.psypo.webshop.repositories.data;

import org.psypo.webshop.data.ThirdPartyOrigin;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * @author ben
 * @version 1.0
 */
@Document
public class Item {

    @Id
    private String id;
    @Indexed
    private String name;
    @Indexed
    private Set<String> tags;
    private String description;
    private Map<ThirdPartyOrigin, String> externalLinks;
    private String previewImageUrl;
    private Collection<String> imageUrls;
    private Date creationDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<String> getTags() {
        return tags;
    }

    public void setTags(Set<String> tags) {
        this.tags = tags;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Map<ThirdPartyOrigin, String> getExternalLinks() {
        return externalLinks;
    }

    public void setExternalLinks(Map<ThirdPartyOrigin, String> externalLinks) {
        this.externalLinks = externalLinks;
    }

    public String getPreviewImageUrl() {
        return previewImageUrl;
    }

    public void setPreviewImageUrl(String previewImageUrl) {
        this.previewImageUrl = previewImageUrl;
    }

    public Collection<String> getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(Collection<String> imageUrls) {
        this.imageUrls = imageUrls;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}
