package org.psypo.webshop.repositories.data.utils;

import org.springframework.security.core.GrantedAuthority;

/**
 * @author ben
 * @version 1.0
 */
public class GrantedAuthorityWrapper implements GrantedAuthority {

    private String authority;

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    @Override
    public String getAuthority() {
        return null;
    }

    public static GrantedAuthorityWrapper factory(String authority) {
        GrantedAuthorityWrapper w = new GrantedAuthorityWrapper();
        w.setAuthority(authority);
        return w;
    }

}
