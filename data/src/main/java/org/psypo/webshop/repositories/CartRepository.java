package org.psypo.webshop.repositories;

import org.psypo.webshop.repositories.data.Cart;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author ben
 * @version 1.0
 */
public interface CartRepository extends MongoRepository<Cart, String> {
}
