package org.psypo.webshop.repositories;

import org.psypo.webshop.repositories.data.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface UserRepository extends MongoRepository<User, String> {

    public User findByUsername(String firstName);

    @Query("{ verificationSent : false }")
    public Page<User> findUsersUnsetVerifications(Pageable pageable);

}