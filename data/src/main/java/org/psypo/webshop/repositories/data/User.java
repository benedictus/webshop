package org.psypo.webshop.repositories.data;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

/**
 * @author ben
 * @version 1.0
 */

@Document
public class User implements UserDetails {

    private String provider;

    public User() {
    }

    public User(String id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.enabled = false;
        this.verificationSent = false;
        this.creationDate = new Date();
        this.userDetails = new UserPersonalDetails();
    }

    @Id
    private String id;

    @Indexed(unique = true)
    private String username;

    private String password;

    private Collection<String> authorities;

    private Boolean enabled;

    private Boolean verificationSent;

    private Date creationDate;

    private UserPersonalDetails userDetails;

    @DBRef
    private Cart cart;

    @Override
    public Collection<? extends SimpleGrantedAuthority> getAuthorities() {
        if (authorities != null) {
            return authorities.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
        }
        return null;
    }

    public void setAuthorities(Collection<String> authorities) {
        this.authorities = authorities;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true; //TODO
    }

    @Override
    public boolean isAccountNonLocked() {
        return true; //TODO
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true; //TODO
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserPersonalDetails getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserPersonalDetails userDetails) {
        this.userDetails = userDetails;
    }

    public Boolean getVerificationSent() {
        return verificationSent;
    }

    public void setVerificationSent(Boolean verificationSent) {
        this.verificationSent = verificationSent;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getProvider() {
        return provider;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }
}