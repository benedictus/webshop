package org.psypo.webshop.repositories.data;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * @author ben
 * @version 1.0
 */
@Document
public class Cart {

    public Cart() {
        products = new ArrayList<>();
        lastUpdated = new Date();
    }

    @Id
    private String id;
    @DBRef
    private Collection<Product> products;
    private Date lastUpdated;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Collection<Product> getProducts() {
        return products;
    }

    public void setProducts(Collection<Product> products) {
        this.products = products;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
}
