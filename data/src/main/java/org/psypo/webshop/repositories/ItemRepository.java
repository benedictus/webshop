package org.psypo.webshop.repositories;

import org.psypo.webshop.repositories.data.Item;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ItemRepository extends MongoRepository<Item,String>{
}
