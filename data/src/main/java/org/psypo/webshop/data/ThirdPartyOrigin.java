package org.psypo.webshop.data;

/**
 * @author ben
 * @version 1.0
 */
public enum ThirdPartyOrigin {
    Facebook, Instagram
}
