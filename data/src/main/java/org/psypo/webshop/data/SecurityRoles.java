package org.psypo.webshop.data;

/**
 * @author ben
 * @version 1.0
 */
public class SecurityRoles {


    public static final String ADMIN_ROLE = "ADMIN";

    public static enum Role {

        admin(ADMIN_ROLE);
        private final String val;

        Role(String val) {
            this.val = val;
        }

        public String getValue() {
            return val;
        }
    }

}

