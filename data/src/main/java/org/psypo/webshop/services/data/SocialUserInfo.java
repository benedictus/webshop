package org.psypo.webshop.services.data;

/**
 * @author ben
 * @version 1.0
 */
public interface SocialUserInfo {
    String getEmail();

    String getImageUrl();

    String getName();

    String getSocialProvider();

    String getId();
}
