package org.psypo.webshop.services;

import org.psypo.webshop.repositories.UserRepository;
import org.psypo.webshop.repositories.data.User;
import org.psypo.webshop.repositories.data.UserPersonalDetails;
import org.psypo.webshop.services.data.SocialUserInfo;
import org.psypo.webshop.services.exceptions.UserCreationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * @author ben
 * @version 1.0
 */
@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepository repository;
    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();


    public User enableUser(String userId) {
        User user = repository.findOne(userId);
        if (user != null) {
            user.setEnabled(true);
            return repository.save(user);
        }
        return null;
    }

    @Override
    public User loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = repository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("No users named " + username + " found");
        }
        return user;
    }

    public PasswordEncoder getPasswordEncoder() {
        return passwordEncoder;
    }

    public boolean usernameExists(String username) {
        return repository.findByUsername(username) != null;
    }

    public User createUser(String email, String password)
            throws UserCreationException {
        User user = new User(null, email, passwordEncoder.encode(password));
        user.setEnabled(false);
        user.setVerificationSent(false);
        User save = repository.save(user);
        if (repository.findOne(save.getId()) == null) {
            throw new UserCreationException();
        }
        return save;
    }

    public User findOne(String s) {
        return repository.findOne(s);
    }

    public User createSocialUser(final SocialUserInfo socialUser)
            throws UserCreationException {
        User user = new User(null, socialUser.getEmail(),
                passwordEncoder.encode(UUID.randomUUID().toString()));
        user.setVerificationSent(true);
        user.setEnabled(true);
        user.setProvider(socialUser.getSocialProvider());
        UserPersonalDetails userDetails = new UserPersonalDetails();
        userDetails.setName(socialUser.getName());
        user.setUserDetails(userDetails);
        User save = repository.save(user);
        if (repository.findOne(save.getId()) == null) {
            throw new UserCreationException();
        }
        return save;
    }
}
