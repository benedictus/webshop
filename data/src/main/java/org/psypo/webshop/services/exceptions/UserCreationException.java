package org.psypo.webshop.services.exceptions;

/**
 * @author ben
 * @version 1.0
 */
public class UserCreationException extends Exception {

    public UserCreationException(String message) {
        super(message);
    }

    public UserCreationException() {
    }
}
