1. Edit webapp/src/main/resources/application.properties
2. Run mongodb, add mongo configuration to application.properties if required
3. Run applications


To run **webapp**:
```
./gradlew :webapp:build :webapp:bootRun
```
To run **admin**:
```
./gradlew :admin:build :admin:bootRun
```

To grant user access for admin module edit user document in mongodb and add
```
"authorities" : [ "ROLE_ADMIN" ]
```
property to your user. Users can be created using **webapp** module


Add items and products in **admin** module to view them in **webapp** webshop